package AerialVehicles;


public class F15 extends Fighters implements IntellgenceAbility{
    private sensorTypes typeSensor ;

    public F15(int numOfRocket, rocketTypes typeRocket , sensorTypes typeSensor) {
        super(numOfRocket, typeRocket);
        this.typeSensor = typeSensor;
    }

    public String getName() {
        return "F15";
    }

    @Override
    public sensorTypes sensor() {
        return typeSensor;
    }
}
