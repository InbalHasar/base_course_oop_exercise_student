package AerialVehicles;


public class Shoval extends Haron implements BdaAbility{
    
    private cameraTypes typeCamera ;

    public Shoval(int numOfRocket, rocketTypes typeRocket, sensorTypes typeSensor ,  cameraTypes typeCamera) {
        super(numOfRocket, typeRocket, typeSensor);
        this.typeCamera = typeCamera;
    }

    public  String getName() {
        return "shoval";
    }

    @Override
    public cameraTypes camera() {
        return typeCamera;
    }
}

