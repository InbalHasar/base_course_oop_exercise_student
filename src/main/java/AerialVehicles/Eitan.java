package AerialVehicles;

public class Eitan extends Haron{

    public Eitan(int numOfRocket, rocketTypes typeRocket, sensorTypes typeSensor) {
        super(numOfRocket, typeRocket, typeSensor);
    }

    public  String getName() {
        return "Eitan";
    }
}
