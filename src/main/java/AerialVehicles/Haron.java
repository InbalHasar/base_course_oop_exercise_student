package AerialVehicles;

public abstract class Haron  extends katmam implements AttackAbility , IntellgenceAbility {

    private static final int hoursToFix = 150;
    private int numOfRocket ;
    private rocketTypes typeRocket ;
    private sensorTypes typeSensor ;

    public Haron(int numOfRocket, rocketTypes typeRocket, sensorTypes typeSensor) {
        this.numOfRocket = numOfRocket;
        this.typeRocket = typeRocket;
        this.typeSensor = typeSensor;
    }

    @Override
    public void check() {
        if (this.getHoursFromFix()>= hoursToFix){
            this.setStatus("not ready");
            this.repair();
        }
        else {
            this.setStatus("ready");
        }
    }

    @Override
    public int rocketAmount() {
        return this.numOfRocket;
    }

    @Override
    public rocketTypes rocket() {
        return typeRocket;
    }

    @Override
    public sensorTypes sensor() {
        return typeSensor;
    }

}
