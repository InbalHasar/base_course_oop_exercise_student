package AerialVehicles;

public interface BdaAbility {
    public static enum cameraTypes{
        REGULAR ,
        THERMAL ,
        NIGHTVISION
    }

    cameraTypes camera();
}
