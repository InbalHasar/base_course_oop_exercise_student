package AerialVehicles;

public interface IntellgenceAbility {

    public static enum sensorTypes{
        INFRARED ,
        ELINT
    }

    sensorTypes sensor();
}
