package AerialVehicles;

public class Kochav extends Hermes implements AttackAbility{
    private int numOfRocket ;
    private rocketTypes typeRocket ;

    public Kochav(sensorTypes typeSensor, cameraTypes typeCamera , int numOfRocket ,rocketTypes typeRocket  ) {
        super(typeSensor, typeCamera);
        this.numOfRocket = numOfRocket;
        this.typeRocket = typeRocket;
    }

    public  String getName() {
        return "kochav";
    }

    @Override
    public int rocketAmount() {
        return this.numOfRocket;
    }

    @Override
    public rocketTypes rocket() {
        return this.typeRocket;
    }
}
