package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.AttackAbility;
import AerialVehicles.BdaAbility;
import Entities.Coordinates;

public class AttackMission extends Mission {

    private String target ;

    public AttackMission(Coordinates coordinates, String namePilot, AerialVehicle aerialVehicle , String target ) throws AerialVehicleNotCompatibleException {
        super(coordinates, namePilot, aerialVehicle);
        if (aerialVehicle instanceof AttackAbility){
            this.target = target ;
        }
        else {
            throw new AerialVehicleNotCompatibleException("an aircraft is not suitable for the mission");
        }
    }

    @Override
    public String executeMission() {
        String message = this.namePilot + " : " +this.getAerialVehicle().getName() + " Attacking " + this.target + " with : "
                +((AttackAbility)this.getAerialVehicle()).rocket()+" X" +((AttackAbility)this.getAerialVehicle()).rocketAmount();
        return message;
    }
}
