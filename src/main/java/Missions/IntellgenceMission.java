package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.IntellgenceAbility;
import Entities.Coordinates;

public class IntellgenceMission extends Mission{

    private String region ;

    public IntellgenceMission(Coordinates coordinates, String namePilot, AerialVehicle aerialVehicle , String region) throws AerialVehicleNotCompatibleException {
        super(coordinates, namePilot, aerialVehicle);
        if (aerialVehicle instanceof IntellgenceAbility){
            this.region = region ;
        }
        else {
            throw new AerialVehicleNotCompatibleException("an aircraft is not suitable for the mission");
        }
    }

    @Override
    public String executeMission() {
        String message = this.namePilot + " : " +this.getAerialVehicle().getName() + " collection data in  " + this.region + " with : "
                +((IntellgenceAbility)this.getAerialVehicle()).sensor();
        return message;
    }
}
