package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission{
    protected Coordinates coordinates ;
    protected String namePilot ;
    protected AerialVehicle aerialVehicle ;

    public Mission(Coordinates coordinates, String namePilot, AerialVehicle aerialVehicle) {
        this.coordinates = coordinates;
        this.namePilot = namePilot;
        this.aerialVehicle = aerialVehicle;
    }

    abstract String executeMission();

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public String getNamePilot() {
        return namePilot;
    }

    public void setNamePilot(String namePilot) {
        this.namePilot = namePilot;
    }

    public AerialVehicle getAerialVehicle() {
        return aerialVehicle;
    }

    public void setAerialVehicle(AerialVehicle aerialVehicle) {
        this.aerialVehicle = aerialVehicle;
    }

    public void begin(){
      this.aerialVehicle.flyTo(this.coordinates);
      System.out.println("Beginning Mission");
    };

    public void cancel(){
        this.aerialVehicle.land(this.coordinates);
        System.out.println("Abort Mission");
    };

    public void finish(){
        this.aerialVehicle.land(this.coordinates);
        System.out.println("Finish Mission");
    };
}
