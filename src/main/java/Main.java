import AerialVehicles.*;
import Entities.Coordinates;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntellgenceMission;

public class Main {

    public static void main(String[] args) throws AerialVehicleNotCompatibleException {
        F15 f15 = new F15(30 , AttackAbility.rocketTypes.PYTHON , IntellgenceAbility.sensorTypes.INFRARED);
        Zik zik = new Zik(IntellgenceAbility.sensorTypes.ELINT , BdaAbility.cameraTypes.REGULAR);
        Kochav kochav = new Kochav(IntellgenceAbility.sensorTypes.INFRARED , BdaAbility.cameraTypes.NIGHTVISION , 20 , AttackAbility.rocketTypes.AMRAM);
        Coordinates coordinates = new Coordinates(4.5 , 31.5);
        AttackMission attackMission = new AttackMission(coordinates,"inbal" , f15 , "gaza") ;
        BdaMission bdaMission = new BdaMission(coordinates , "aviv" , kochav , "jordan");
        IntellgenceMission intellgenceMission = new IntellgenceMission(coordinates , "nir" , zik , "egypt");
        System.out.println(attackMission.executeMission());
        System.out.println(bdaMission.executeMission());
        System.out.println(intellgenceMission.executeMission());
    }


}
